//routing scripting 
Router.configure({
	layoutTemplate:'appLayout'
});

Router.route('/',function(){
	this.render('navbar',{
		to:'navbar'
	});
	this.render('post',{
		to:'section'
	});
});
/////////////////////////////////
Router.route("/new_post",function(){
 this.render("navbar",{
     to:"navbar"
 });

this.render("newp",{
    to:"section"
  });
});
/////////////////////////////////
Router.route("/all_posts",function(){
 this.render("navbar",{
     to:"navbar"
 });

this.render("all_posts",{
    to:"section"
  });
});
///////////////////////////////////

Router.route("/about_us",function(){
 this.render("navbar",{
     to:"navbar"
 });

this.render("about_us",{
    to:"section"
  });
});
///////////////////////////////////

Router.route("/notifications",function(){
 this.render("navbar",{
     to:"navbar"
 });

this.render("notifications",{
    to:"section"
  });
});
///////////////////////////////////
Router.route("/my_recipes",function(){
 this.render("navbar",{
     to:"navbar"
 });

this.render("my_recipes",{
    to:"section"
  });
});
///////////////////////////////////

/////////////////////////////////////

Router.route('/my_favorites',function(){
	this.render("navbar",{
	   to:'navbar'
	   	
	});
  this.render("my_favorites",{
	 to:"section"
	})	
	
});
//////////////////////////////////

Router.route('/single/:_id',function(){
	this.render("navbar",{
	   to:'navbar'
	   	
	});
  this.render("single_post",{
	 to:"section",
	 data:function(){
		   return Data.findOne({_id:this.params._id});
		 }
	})	
	
});
Router.route('/register',function(){
 this.render("register",{
	  to:'navbar'
	 })
	
})
